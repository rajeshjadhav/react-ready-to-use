const initialState = {
    companiesListData: [],
    // companiesListData: [
    //     {
    //         name: 'Company Name 1',
    //         description: 'Company Description'
    //     },
    //     {
    //         name: 'Company Name 2',
    //         description: 'Company Description'
    //     }
    // ],
};

const companies = (state = initialState, action) => {
    
    switch (action.type) {
        case 'GET_ALL_COMAPNIES':
            return Object.assign({}, state, action.data);            
        case 'ADD_COMPANY':
            return state;
        case 'UPDATE_COMPANY':
            return state;
        case 'DELETE_COMPANY':
            return state;
        default:
            return state;
    }
}

export default companies;