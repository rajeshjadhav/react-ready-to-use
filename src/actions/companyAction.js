export const action1 = () => {
    return (dispatch) => {
        return fetch('https://reqres.in/api/users?page=2')
            .then(res => res.json())
            .then(response => {
                // console.log('response:', response);
            });
    }
}
export const getAllCompanies = () => {
    return dispatch => {
        const initialState = [
            {
                name: 'Cybage',
                description: 'Cybage Software is an information technology consulting company founded in 1995 in Kalyani Nagar, Pune, India. The company provides software application development and maintenance, with development centers in Pune, Hyderabad and Gandhinagar in India.'
            },
            {
                name: 'Cybage',
                description: 'Cybage Software is an information technology consulting company founded in 1995 in Kalyani Nagar, Pune, India. The company provides software application development and maintenance, with development centers in Pune, Hyderabad and Gandhinagar in India.'
            }
        ];
        dispatch({ type: 'GET_ALL_COMAPNIES', 
            data: { companiesListData: initialState } 
        });
    }
}