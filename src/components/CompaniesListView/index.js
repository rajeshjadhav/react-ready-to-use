import React from "react";
import { Paper, Grid } from '@material-ui/core/';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const styles = {
    card: {
        minWidth: 275,
        maxWidth: 275,
        marginLeft: 5,
        marginRight: 5
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        marginBottom: 16,
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
};

const CompaniesListView = ({companiesListData, classes}) => {
    console.log('[ companiesListData ]', companiesListData);

    const companiesListElement = companiesListData.map((company, i) =>
        <Card className={classes.card} key={i}>
            <CardContent>
                <Typography variant="headline" component="h2">
                    { company.name }
                </Typography>
                <Typography component="p">
                    { company.description }
                </Typography>
            </CardContent>
        </Card>
    );
    console.log('[ companiesListElement ]', companiesListElement);   

    return (
        <Grid container item justify='center'>
            <Grid container item xs={8} >
                {companiesListElement}
            </Grid>
        </Grid>
    );
}

export default withStyles(styles)(CompaniesListView);