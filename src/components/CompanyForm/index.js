import React from "react";
import { Button, Paper, Grid } from '@material-ui/core/';

const Test = () => {
    return (
        <Grid container item justify='center'>
            <Grid container item xs={8} >
                <Paper>
                    <Button variant="contained" color="primary">
                        Submit
                    </Button>
                </Paper>
            </Grid>
        </Grid>
    );
}
export default Test;