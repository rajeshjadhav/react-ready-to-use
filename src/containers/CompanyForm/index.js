import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from "redux";
import CompanyFormComponent from '../../components/CompanyForm/';
import * as companyActionCreator from '../../actions/companyAction';

class CompanyForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    componentDidMount(){
        this.props.companyAction.action1();
    }
    render() {
        return (
            <div className="App">
                <CompanyFormComponent />
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    testState: state.testData
})

const mapDispatchToProps = (dispatch) => ({
    companyAction: bindActionCreators(companyActionCreator, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(CompanyForm);
// export default withRouter(connect(mapStateToProps,mapDispatchToProps)(Test));