import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from "redux";
import CompaniesListViewComponent from '../../components/CompaniesListView/';
import * as companyActionCreator from '../../actions/companyAction.js';

class CompaniesListView extends React.Component {
    
    componentWillMount() {
        const { companyAction } = this.props;
        companyAction.getAllCompanies();
    }
    render() {
        return (
            <div className="App">
                <CompaniesListViewComponent {...this.props.companiesListData }/>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    companiesListData: state.companiesListData
})

const mapDispatchToProps = (dispatch) => ({
    companyAction: bindActionCreators(companyActionCreator, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(CompaniesListView);
// export default withRouter(connect(mapStateToProps,mapDispatchToProps)(Test));