import React from "react";
import { Provider } from "react-redux";
import BrowserRouter from "react-router-dom/BrowserRouter";
import store from "../store/";
import CompanyForm from "../containers/CompanyForm/";
import CompaniesListView from "../containers/CompaniesListView/";

const AppRouter = () => {
    return (
        <BrowserRouter>
            <div>
                <Provider store={store}>
                    <React.Fragment>
                        <CompanyForm />
                        <CompaniesListView />
                    </React.Fragment>
                </Provider>
            </div>
        </BrowserRouter>
    );
}
export default AppRouter;