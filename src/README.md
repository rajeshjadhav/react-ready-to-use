# company-data-assignment
- Add/update/delete company 
    - fields
        - name
        - description
- list all comapnies with description

- actions   
    - companyAction
        - ADD_COMPANY    
        - UPDATE_COMPANY
        - DELETE_COMPANY
        - GET_ALL_COMAPNIES
- components
    - CompanyForm
    - CompaniesListView
- 