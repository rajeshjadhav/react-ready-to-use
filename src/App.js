import React from 'react';
import CompanyForm from './containers/CompanyForm/';
import CompaniesListView from './containers/CompaniesListView/';
import AppRouter from './routers/AppRouter.js';


class App extends React.Component {
  render() {
    return (
      <div className="App">
        <AppRouter>
          <div>
            <CompanyForm />
            <CompaniesListView />
          </div>
        </AppRouter>
      </div>
    );
  }
}

export default App;